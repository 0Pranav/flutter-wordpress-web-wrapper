import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'ArticleList.dart';
import 'colors.dart';

class Category {
  static List data;
}

void main() async {
  var response = await http.get(
      Uri.encodeFull("https://iosapptemplates.com/wp-json/wp/v2/categories"));
  Category.data = jsonDecode(response.body);
  runApp(MaterialApp(
    home: WordpressReader(),
  ));
}

class WordpressReader extends StatefulWidget {
  @override
  _WordpressReaderState createState() => _WordpressReaderState();
}

class _WordpressReaderState extends State<WordpressReader>
    with SingleTickerProviderStateMixin  {
  TabController _animationController;

  @override
  void initState() {
    _animationController = TabController(vsync: this, length: 2);
    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavigationDrawer(),
      appBar: AppBar(
        elevation: defaultTargetPlatform == TargetPlatform.iOS ? 0 : 5 ,
        title: Text("News"),
        centerTitle: defaultTargetPlatform == TargetPlatform.iOS,
        backgroundColor: AppSpecificColors.navigationBarColor(),
        bottom: TabBar(
            controller: _animationController,
            indicatorColor: AppSpecificColors.orangeHighlightedColor(),
            tabs: <Tab>[
              Tab(
                text: 'Top Posts',
              ),
              Tab(
                text: 'All Posts',
              )
            ]),
      ),
      body: TabBarView(
        controller: _animationController,
        children: <Widget>[
          Container(
            child: ArticleListView(
                "https://iosapptemplates.com/wp-json/wp/v2/posts"),
          ),
          Container(
            child: ArticleListView(
                "https://iosapptemplates.com/wp-json/wp/v2/posts"),
          )
        ],
      ),
    );
  }
}

class NavigationDrawer extends StatefulWidget {
  @override
  _NavigationDrawerState createState() => _NavigationDrawerState();
}

class _NavigationDrawerState extends State<NavigationDrawer> {
  List data;

  _NavigationDrawerState() {
    this.data = Category.data;
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      elevation: defaultTargetPlatform == TargetPlatform.iOS ? 0 : 16,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 40),
        color: AppSpecificColors.darkBackgroundColor(),
        child: ListView.separated(
          separatorBuilder: (context,i){return Divider(color: AppSpecificColors.seperatorColor(),);},
            itemCount: data == null ? 0 : data.length + 1,
            itemBuilder: (context, i) {
              if (i == 0)
                return GestureDetector(
                  child: Padding(padding: EdgeInsets.symmetric(vertical: 5,horizontal: 16),child: Text("Home",style: TextStyle(color: Colors.white,fontSize: 18)),),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ); else {
                var id = data[i - 1]["id"];
                return GestureDetector(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10,horizontal: 16),
                      child: Text(
                        data[i - 1]["name"],
                        style: TextStyle(color: Colors.white,fontSize: 18),
                      ),
                    ),
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return Scaffold(
                          appBar: AppBar(
                            title: Text(data[i - 1]["name"]),
                            backgroundColor:
                                AppSpecificColors.navigationBarColor(),
                          ),
                          body: ArticleListView(
                              "https://iosapptemplates.com/wp-json/wp/v2/posts?categories=${id}"),
                        );
                      }));
                    });
              }
            }),
      ),
    );
  }
}
