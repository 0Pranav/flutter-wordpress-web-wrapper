import 'package:flutter/material.dart';

class AppSpecificColors {
  static Color navigationBarColor(){
    return Color.fromARGB(255, 51, 51, 51);
  }
  static Color menuBackgroundColor() {
    return Color.fromARGB(255, 39, 44, 48);
  }

  static Color seperatorColor() {
    return Color.fromARGB(255, 90, 90, 90);
  }

  static Color orangeHighlightedColor() {
    return Color.fromARGB(255, 239, 135, 50);
  }

  static Color darkBackgroundColor() {
    return Color.fromARGB(255, 68, 68, 68);
  }

  static Color unselectedTextColor() {
    return Color.fromARGB(255, 204, 204, 204);
  }
}
