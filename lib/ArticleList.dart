import 'dart:convert';
import 'package:html_unescape/html_unescape.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:http/http.dart' as http;

import 'colors.dart';

class ArticleListView extends StatefulWidget {
  String uri = "";

  ArticleListView(String uri) {
    if (uri.contains("?categories"))
      uri = uri + "&page=";
    else
      uri = uri + "?page=";
    this.uri = uri;
  }

  @override
  _ArticleListViewState createState() => _ArticleListViewState(uri);
}

class _ArticleListViewState extends State<ArticleListView>
    with AutomaticKeepAliveClientMixin<ArticleListView> {
  List articleList;
  var uri;
  int page = 1;
  ScrollController _scrollController = new ScrollController();

  @override
  bool get wantKeepAlive => true;
  _ArticleListViewState(String uri) {
    if (uri == "") {
      throw new FormatException("wrong uri");
    } else {
      this.uri = uri;
    }
  }

  @override
  initState() {
    super.initState();
    getNextArticles();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        page++;
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text("Loading more articles"),
        ));
        getNextArticles();
      }
    });
  }

  Future<String> getNextArticles() async {
    var response = await http.get(Uri.encodeFull(this.uri + page.toString()),
        headers: {"Accepts": "application/json"});

    if (response.statusCode == 200) {
      setState(() {
        if (articleList == null) {
          articleList = (jsonDecode(response.body));
        } else {
          articleList.addAll(jsonDecode(response.body));
        }
        Scaffold.of(context).hideCurrentSnackBar();
      });
    }

    return "Success";
  }

  @override
  Widget build(BuildContext context) {
    var unescape = HtmlUnescape();
    print(articleList);
    if (articleList == null) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else {
      return ListView.separated(
          controller: _scrollController,
          separatorBuilder: (context, index) {
            return Divider(
              color: AppSpecificColors.seperatorColor(),
            );
          },
          itemCount: /*articleList == null ? 0 :*/ articleList.length,
          itemBuilder: (context, i) {
            if (articleList == null)
              return Center(
                child: Text("There's a problem with the internet"),
              );
            else
              return GestureDetector(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return WebviewScaffold(
                      initialChild: Center(
                        child: CircularProgressIndicator(),
                      ),
                      url: articleList[i]['link'],
                      appBar: AppBar(
                        title: Text(articleList[i]['title']['rendered']),
                        backgroundColor: AppSpecificColors.navigationBarColor(),
                      ),
                    );
                  }));
                },
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          '⌚ ' + getDate(articleList[i]['date']),
                          style: TextStyle(
                              color: AppSpecificColors.unselectedTextColor()),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 5.0),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                  child: Text(
                                    unescape.convert(
                                        articleList[i]['title']['rendered']),
                                        style: TextStyle(fontSize: 18),
                                  ),
                                
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 8.0),
                                child: Image.network(
                                  "http://lorempixel.com/800/500/",
                                  height: 58,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
          });
    }
  }

  String getDate(String rawDate) {
    Duration timeElapsed = DateTime.now().difference(DateTime.parse(rawDate));
    if (timeElapsed.inDays >= 365)
      return (timeElapsed.inDays / 365).round().toString() + " years ago";
    if (timeElapsed.inDays >= 30)
      return (timeElapsed.inDays / 30).round().toString() + " months ago";
    if (timeElapsed.inDays > 1)
      return timeElapsed.inDays.toString() + " days ago";
    if (timeElapsed.inHours > 1)
      return timeElapsed.inHours.toString() + " hours ago";
    if (timeElapsed.inMinutes >= 1)
      return timeElapsed.inMinutes.toString() + " minutes ago";
    return "Few seconds ago";
  }
}
